from flask import session, redirect, jsonify
from models import *
from slackbot_credits import *

from functools import wraps

import json, requests


class SlackAuthFailed(Exception):
    """Exception for cases when slack auth is failed"""


class SlackAPIWrapper(object):
    __oauth_urls = {
        "OAUTH_AUTHENTICATION_URL": "https://slack.com/oauth/authorize",
        "OAUTH_TEST_URL": 'https://slack.com/api/auth.test',
        "OAUTH_ACCESS_TOKEN": 'https://slack.com/api/oauth.access',
        "OAUTH_USER_INFO": "https://slack.com/api/users.info"
    }
    __messaging_urls = {
        'POST_MESSAGE_URL': 'https://slack.com/api/chat.postMessage',
        'GET_CHANNELS_LIST': 'https://slack.com/api/channels.list',
        'GET_GROUPS_LIST': 'https://slack.com/api/groups.list',
        'POST_BOT_TO_CHANNEL': 'https://slack.com/api/channels.invite',
        'POST_BOT_TO_GROUP': 'https://slack.com/api/groups.invite',
        'KICK_BOT_FROM_CHANNEL': 'https://slack.com/api/channels.kick',
        'KICK_BOT_FROM_GROUP': 'https://slack.com/api/groups.kick',
        'GET_LIST_OF_USERS': 'https://slack.com/api/users.list'
    }

    @staticmethod
    def auth_redirect_handler(code):
        '''
        recieves the access_token from grant code
		'''

        params = {
            'client_id': SLACK_APP_ID,
            'client_secret': SLACK_APP_SECRET,
            'code': code,
            'redirect_uri': SLACK_APP_REDIRECT_URI
        }

        return json.loads(requests.get(SlackAPIWrapper.__oauth_urls['OAUTH_ACCESS_TOKEN'], params=params).text)

    @staticmethod
    def get_team():
        ''' returns team of signed in user. Takes team id from session
		'''
        team_id = session['slack_team']
        o = Team.objects(team_id=team_id).first()
        return o

    @staticmethod
    def check_user(access_token):
        ''' checks the user permissions. Only admins and team owners
			are allowed to perform operations
		'''
        user_info = SlackAPIWrapper.get_user_data(access_token)
        return user_info['is_admin'] == True or user_info['is_owner'] == True

    @staticmethod
    def get_check_user(access_token):
        ''' checks the user permissions. Only admins and team owners
            are allowed to perform operations
        '''
        user_info = SlackAPIWrapper.get_user_data(access_token)
        return user_info

    @staticmethod
    def authentication_url():
        return SlackAPIWrapper.__oauth_urls['OAUTH_AUTHENTICATION_URL'] + ('?scope=users:read,channels:write,channels:read,groups:read,groups:write&client_id=%s&redirect_uri=%s' % (SLACK_APP_ID, SLACK_APP_REDIRECT_URI))

    @staticmethod
    def installation_url():
        return SlackAPIWrapper.__oauth_urls['OAUTH_AUTHENTICATION_URL'] + (
                '?scope=bot,users:read,channels:write,channels:read,groups:read,groups:write&client_id=%s&redirect_uri=%s' % (
            SLACK_APP_ID, SLACK_APP_REDIRECT_URI))

    @staticmethod
    def get_channels_list(client):
        response_channel = requests.get(SlackAPIWrapper.__messaging_urls['GET_CHANNELS_LIST'], params={
            'token': session['slack_user']
        })
        response_groups = requests.get(SlackAPIWrapper.__messaging_urls['GET_GROUPS_LIST'], params={
            'token': session['slack_user']
        })
        response_channel = json.loads(response_channel.text)
        response_groups = json.loads(response_groups.text)

        channels = response_channel['channels']
        groups = response_groups['groups']
        team = Team.objects(team_id=client['team_id']).first()

        # channels = filter(lambda x: team['bot_id'] in x['members'], channels)
        # groups = filter(lambda x: team['bot_id'] in x['members'], groups)

        return {'channels': channels + groups, 'selected': team['channel_id'] if 'channel_id' in team else None}

    @staticmethod
    def send_message(client, text):
        r = requests.post(SlackAPIWrapper.__messaging_urls['POST_MESSAGE_URL'], params={
            'text': text,
            'as_user': True,
            'channel': client['channel_id'],
            'token': client['bot_token']
        })
        res = r.json()
        return res['ok']  # bool

    @staticmethod
    def get_user_data(access_token):
        user = json.loads(requests.get(SlackAPIWrapper.__oauth_urls['OAUTH_TEST_URL'], params={'token': access_token}).text)

        if 'error' in user:
            raise SlackAuthFailed

        user_id = user['user_id']

        user_info = requests.get(SlackAPIWrapper.__oauth_urls['OAUTH_USER_INFO'], params={'token': access_token, 'user': user_id})
        user_info = json.loads(user_info.text)['user']
        return user_info

    @staticmethod
    def post_bot_to_group_or_channel(access_token, channel_id, bot_id):
        params = {
            'token': access_token,
            'channel': channel_id,
            'user': bot_id
        }
        r = requests.post(SlackAPIWrapper.__messaging_urls['POST_BOT_TO_CHANNEL'], params=params)
        res = r.json()
        if not res['ok']:
            r = requests.post(SlackAPIWrapper.__messaging_urls['POST_BOT_TO_GROUP'], params=params)
            res = r.json()
        return res['ok']

    @staticmethod
    def remove_bot_from_group_or_channel(access_token, channel_id, bot_id):
        params = {
            'token': access_token,
            'channel': channel_id,
            'user': bot_id
        }
        r = requests.post(SlackAPIWrapper.__messaging_urls['KICK_BOT_FROM_CHANNEL'], params=params)
        res = r.json()
        if not res['ok']:
            r = requests.post(SlackAPIWrapper.__messaging_urls['KICK_BOT_FROM_GROUP'], params=params)
            res = r.json()
        return res['ok']

    @staticmethod
    def get_users_list(access_token):
        params = {
            'token': access_token,
            'limit': 100
        }
        members = []
        while True:
            res = requests.get(SlackAPIWrapper.__messaging_urls['GET_LIST_OF_USERS'], params=params).json()
            if not res['ok']:
                raise Exception(res)
            members.extend(res['members'])
            cursor = ''
            if 'response_metadata' in res and 'next_cursor' in res['response_metadata']:
                cursor = res['response_metadata']['next_cursor']
            if cursor:
                params.update({'cursor': cursor})
                continue
            else:
                break
        return members

    @staticmethod
    def get_user_id_by_name(access_token, user_name):
        members = SlackAPIWrapper.get_users_list(access_token)
        user_name = user_name[1:] if user_name.startswith('@') else user_name
        keys = ['display_name', 'real_name', 'real_name_normalized', 'display_name_normalized', 'email']
        for key in keys:
            users = list(filter(lambda user: user.get('profile') and user['profile'].get(key) and user['profile'][key] == user_name, members))
            if len(users) > 0:
                return users[0]['id']
        return user_name

    @staticmethod
    def authorized(f):
        ''' decorator for wrapping the endpoint with security rules
		'''

        @wraps(f)
        def _(*args, **kwargs):
            try:
                if 'slack_user' in session:
                    if SlackAPIWrapper.check_user(session['slack_user']):
                        return f()
                    else:
                        return jsonify(**{'fail': True})
                else:
                    # TODO: Separate application/json and html error handling
                    return redirect(SlackAPIWrapper.authentication_url())
            except SlackAuthFailed, e:
                return redirect(SlackAPIWrapper.authentication_url())

        return _
