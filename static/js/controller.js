
var app = angular.module('client', []);

app.controller('controller', function ($scope, $http) {
	$scope.getCalendars = function() {
		$http({
			method: 'GET',
			url: '/api/calendar'
		}).success(function(data) {
			$scope.selectedCalendar = data.data.selected;
			$scope.calendars = data.data.calendars;
			$scope.loadingData[1] = false;
		})
	};
	
	$scope.getCongratulations = function() {
		$http({
			method: 'GET',
			url: '/api/congrats'
		}).success(function(data, status) {
			$scope.congrats = data.congrats;
			$scope.loadingData[0] = false;
		})
	};

	$scope.createCongratulation = function() {
		$http({
			method: 'POST',
			data: { text: $scope.congrat_text },
			url: '/api/congrats'
		}).success(function(data, status) {
			$scope.congrats.push(data);
			$scope.congrat_text = '';
		})
	};

	$scope.dropCongratulation = function(id, type) {
		$http({
			method: 'DELETE',
			url: '/api/congrats',
			params: {id: id, type: type}
		}).success(function(data){
			var ids = $scope.congrats.map(function(d){return d.id});
			$scope.congrats.splice(ids.indexOf(id), 1);
		})
	};

	$scope.getChannels = function() {
		$http({
			method: 'GET',
			url: '/api/channel'
		}).success(function(data) {
			$scope.selectedChannel = data.data.selected;
			$scope.channels = data.data.channels;
			$scope.loadingData[2] = false;
		})
	};

	$scope.update = function() {
		$('.save-button').html('Saving...').prop('disabled', true);
		$http({
			method:'POST',
			url: '/api/update',
			data: {
				calendar_id: $scope.selectedCalendar,
				channel_id: $scope.selectedChannel,
				time_hours: $scope.timeHour,
				time_minutes: $scope.timeMinute,
				time_zone: $scope.timeZone
			}
		}).success(function(data) {
			console.log('Config successfully saved');
			$('.save-button').html('SAVE').prop('disabled', false);
			$('.config-saved').fadeIn(300).delay(8000).fadeOut(300);
		})
	};

	$scope.getTimes = function() {
		$http({
			method: 'GET',
			url: '/api/time'
		}).success(function(data, status) {
			$scope.timeHour = data.hour;
			$scope.timeMinute = data.minute;
			$scope.timeZone = data.zone;
			$scope.loadingData[3] = false;
		})
	};

	$scope.setSelected = function(calendar) {
		$scope.selectedCalendar = calendar
	};
	$scope.setSelectedChannel = function(channel) {
		$scope.selectedChannel = channel
	};

	$scope.loadingData = [true, true, true, true];

	$scope.getCongratulations();
	$scope.getCalendars();
	$scope.getChannels();
	$scope.getTimes();

	$scope.timeHourOptions = [];
	for (var i = 0; i < 24; i++) {
		$scope.timeHourOptions.push({'name': (i < 10 ? '0' : '') + i, 'val': i});
	}
	$scope.timeMinuteOptions = [];
	for (i = 0; i < 60; i++) {
		$scope.timeMinuteOptions.push({'name': (i < 10 ? '0' : '') + i, 'val': i});
	}
	$scope.timeZoneOptions = [];
	for (i = -12; i < 13; i++) {
		$scope.timeZoneOptions.push(i);
	}
});
