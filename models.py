import datetime
from mongoengine import *

connect('birthday_bot_data')


class Team(Document):
    calendar_id = StringField()
    team_id = StringField()
    channel_id = StringField()
    team_name = StringField()
    bot_token = StringField()
    bot_id = StringField()
    google_access_token = StringField()
    google_refresh_token = StringField()
    time_hours = IntField(min_value=0, max_value=23, default=9)
    time_minutes = IntField(min_value=0, max_value=59, default=0)
    time_zone = IntField(min_value=-12, max_value=12, default=0)


class Congrats(Document):
    text = StringField()
    team = ReferenceField(Team)


class Events(Document):
    text = StringField()
    team = ReferenceField(Team)


class SentMessages(Document):
    team = ReferenceField(Team)
    calendar_message = StringField()
    date = DateTimeField(default=datetime.datetime.now)
