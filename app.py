import os
from flask import *
from models import *
from tools import *
from flask_bower import Bower

app = Flask(__name__)

Bower(app)

# TODO: join all data receiving into one view

app.config['SECRET_KEY'] = 'random-string'


@app.route('/')
def home_page():
    return render_template('index.html')


@app.route('/auth/slack')
def slack_auth_redirect():
    code = request.args['code']

    response = slack.auth_redirect_handler(code)

    # return jsonify(**{'res': response, 'url': slack.authentication_url()})

    if not slack.check_user(response['access_token']):
        # TODO: handle with 404 page
        return jsonify(**{'lol': 'fail'})

    user_data = slack.get_user_data(response['access_token'])

    team = Team.objects(team_id=user_data['team_id'])

    if len(team) == 0:
        if 'bot' not in response:
            return redirect(slack.installation_url())

        channel_id = None

        if 'incoming_webhook' in response and 'channel_id' in response['incoming_webhook']:
            channel_id = response['incoming_webhook']['channel_id']

        team = Team(team_id=response['team_id'], team_name=response['team_name'],
                    bot_token=response['bot']['bot_access_token'], bot_id=response['bot']['bot_user_id'],
                    channel_id=channel_id)
        team.save()

        if channel_id:
            slack.post_bot_to_group_or_channel(response['access_token'], team['channel_id'], team['bot_id'])
    else:
        if 'bot' in response:
            team.update(bot_token=response['bot']['bot_access_token'], bot_id=response['bot']['bot_user_id'])

    session['slack_team'] = response['team_id']
    session['slack_user'] = response['access_token']
    return redirect('/config')


@app.route('/auth/google')
def google_auth_redirect():
    code = request.args['code']
    team_id = session['slack_team']

    response = google_api.auth_redirect_handler(code)

    o = Team.objects(team_id=team_id)

    if len(o) != 0:
        o = o[0]
        if 'refresh_token' in response and response['refresh_token']:
            o.update(google_refresh_token=response['refresh_token'],
                     google_access_token=response['access_token'])
        else:
            o.update(google_access_token=response['access_token'])
        o.save()

    return redirect('/config')


@app.route('/api/profile')
@slack.authorized
def get_user_data():
    return slack.get_user_data(session['slack_user'])


@app.route('/config')
@slack.authorized
@google_api.authorized
def render_select_calendar_template():
    return render_template('config-template.html')


@app.route('/config/remove')
@slack.authorized
@google_api.authorized
def render_config_remove_template():
    user = slack.get_user_data(session['slack_user'])
    team = Team.objects(team_id=user['team_id'])[0]
    congrats = Congrats.objects(team=team)
    for congr in congrats:
        Congrats.delete(congr)
    Team.delete(team)
    session.clear()
    return render_template('remove-template.html')


@app.route('/api/slack/events', methods=['GET', 'POST'])
def help_event():
    if request.method == 'POST':
        req = request.get_json()
        if 'challenge' in req:
            return make_response(jsonify({'challenge': req['challenge']}))
        status = False
        if 'event' in req and (req['event']['type'] == 'message' and
                               req['event']['text'].lower() in ('help', 'help me', 'i need help')):
            with open(os.path.join(app.root_path, 'help.txt'), 'r') as f:
                help_msg = f.read()

            channel_id = req['event']['channel']
            team = Team.objects(team_id=req['team_id'])[0]
            status = slack.send_message({'channel_id': channel_id, 'bot_token': team['bot_token']}, help_msg)
        return make_response(jsonify({'status': status}))
    else:
        return make_response(jsonify({'lol': 'fail'}))


@app.route('/send_congrats', methods=['GET'])
def send_congrats():
    from bot_core.bot import detect_birthday
    detect_birthday()
    return make_response(jsonify({'status': 'ok'}))

"""
@app.route('/saved/messages/get')
def render_remove_team_template():
    msg = SentMessages.objects.all()
    return render_template('tmp-template.html', msg=msg)

@app.route('/config/session/clear')
@slack.authorized
@google_api.authorized
def render_session_clear_template():
	user = slack.get_user_data(session['slack_user'])
	team = Team.objects(team_id=user['team_id'])
	if len(team) > 0:
		session.clear()
	return redirect('/')

TODO: remove after tests!!!!
@app.route('/remove/<teamname>')
def render_remove_team_template(teamname):
	teams = Team.objects(team_name=teamname)
	if len(teams) > 0:
		for t in teams:
			Team.delete(t)
	return redirect('/')
"""


@app.route('/api/congrats', methods=['GET', 'POST', 'DELETE'])
@slack.authorized
def get_congrats():
    user = slack.get_user_data(session['slack_user'])
    team = Team.objects(team_id=user['team_id'])
    team = team[0]

    if request.method == 'GET':
        congrats = Congrats.objects(team=team).as_pymongo()
        events = Events.objects(team=team).as_pymongo()
        data = []
        data.extend(map(lambda x: {'text': x['text'], 'id': str(x['_id']), 'type': 'birthday'}, congrats))
        data.extend(map(lambda x: {'text': x['text'], 'id': str(x['_id']), 'type': 'event'}, events))
        return jsonify(**{'congrats': data}), 200

    if request.method == 'POST':
        text = request.json['text']
        if '<{}>' in text:
            c = Congrats(team=team, text=text).save()
            type = 'birthday'
        elif '{}' in text:
            c = Events(team=team, text=text).save()
            type = 'event'
        else:
            return jsonify(**{'id': None, 'text': 'Can\'t find right template'}), 400
        return jsonify(**{'id': str(c.id), 'text': c['text'], 'type': type}), 200

    if request.method == 'DELETE':
        type = request.args['type']
        if type == 'birthday':
            Congrats(id=request.args['id']).delete()
        else:
            Events(id=request.args['id']).delete()
        return jsonify(**{'status': 'ok'})


@app.route('/api/update', methods=['POST'])
@slack.authorized
def update():
    team = slack.get_team()
    data = request.json
    team.update(**data)
    if team['channel_id'] != data['channel_id']:
        slack.remove_bot_from_group_or_channel(session['slack_user'], team['channel_id'], team['bot_id'])
        slack.post_bot_to_group_or_channel(session['slack_user'], data['channel_id'], team['bot_id'])
    return jsonify(**{'status': 'ok'})


@app.route('/api/calendar', methods=['POST', 'GET'])
@slack.authorized
def get_calendars():
    team = slack.get_team()
    wrapper = google_api(team)

    ls = {
        'calendars': wrapper.get_calendar_list()['items'],
        'selected': team['calendar_id'] if 'calendar_id' in team else None
    }

    return jsonify(**{'data': ls})


@app.route('/api/channel')
@slack.authorized
def get_channels():
    user = slack.get_user_data(session['slack_user'])
    channels = slack.get_channels_list(user)
    team = Team.objects(team_id=user['team_id']).first()
    channels['user_chanel'] = team['channel_id']
    return jsonify(**{'data': channels})


@app.route('/api/time')
@slack.authorized
def get_times():
    user = slack.get_user_data(session['slack_user'])
    team = Team.objects(team_id=user['team_id']).first()
    return jsonify(**{
        'hour': team.time_hours if team.time_hours else 9,
        'minute': team.time_minutes if team.time_minutes else 0,
        'zone': team.time_zone if team.time_zone else 0
    })


if __name__ == '__main__':
    # app.run(debug=True)
    app.run(debug=False)
