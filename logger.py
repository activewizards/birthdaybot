import datetime
import os
import json

log_file = 'birthday_logs.log'
file = os.path.join(os.path.dirname(__file__), log_file)


def log_message(message):
    with open(file, 'a') as log:
        log.write('{time} : {message}\n\n'.format(
            time=datetime.datetime.now().replace(microsecond=0),
            message=message
        ))


def log_message_json(obj):
    with open(file, 'a') as log:
        log.write('{time} :\n{message}\n\n'.format(
            time=datetime.datetime.now().replace(microsecond=0),
            message=json.dumps(obj, indent=4, sort_keys=True)
        ))
