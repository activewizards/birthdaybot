# coding=utf-8
# -*- coding utf-8 -*-
import datetime, random, models

from tools import *
from settings import *
from datetime import datetime, timedelta
import logging

logger = logging.getLogger()
logger.setLevel(logging.INFO)

@app.task(ignore_result=True)
def detect_birthday():
    credits = models.Team.objects
    for credit in credits:
        logger.info('TEAM: {} | {} {} {}'.format(credit['team_id'], credit['time_hours'],
                                                 credit['time_minutes'], credit['time_zone']))
        try:
            callendar = google_api(credit)
            events = callendar.get_events(credit['calendar_id'])
            for event in events:
                text = event['summary']
                unow = datetime.utcnow()
                if credit['time_zone'] >= 0:
                    unow += timedelta(hours=abs(credit['time_zone']))
                else:
                    unow -= timedelta(hours=abs(credit['time_zone']))

                if unow.hour > credit['time_hours'] or (unow.hour == credit['time_hours']
                                                        and unow.minute >= credit['time_minutes']):
                    min_time = unow.replace(hour=0, minute=0, second=0, microsecond=0)
                    max_time = unow.replace(hour=23, minute=59, second=59, microsecond=999999)
                    sent_msg = models.SentMessages.objects(team=credit,
                                                           calendar_message=event['summary'],
                                                           date__lt=max_time,
                                                           date__gt=min_time).count()
                    logger.info('EVENT: {} | '.format(event['summary']))
                    if sent_msg == 0:
                        congrats = models.Congrats.objects(team=credit) if event['birthday'] \
                            else models.Events.objects(team=credit)
                        if len(congrats) is 0: continue
                        random_num = random.randint(0, len(congrats) - 1)
                        if event['birthday']:
                            user = slack.get_user_id_by_name(credit['bot_token'], text)
                            text = '@'+user
                        status = slack.send_message(credit, congrats[random_num].text.format(text))
                        if status:
                            models.SentMessages(team=credit, calendar_message=event['summary'], date=unow).save()
        except GoogleRefreshTokenRequired:
            logger.error('REFRESH TOKEN ERROR {}'.format(credit['team_id']))
            pass
        except Exception as e:
            logger.error(e)
            pass


if __name__ == '__main__':
    detect_birthday()
