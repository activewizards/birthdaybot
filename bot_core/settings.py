#!/usr/bin/python
# -*- coding: utf-8 -*-

# broker url for celery worker
BROKER_URL = 'mongodb://localhost:27017/birthday_bot'

# START : CELERY config
from celery import Celery
from celery.schedules import crontab

app = Celery('bot', broker=BROKER_URL)

app.conf['CELERY_ACCEPT_CONTENT'] = ['application/json']
app.conf['CELERY_TASK_SERIALIZER'] = 'json'
app.conf['CELERY_RESULT_SERIALIZER'] = 'json'
app.conf['CELERY_ACCEPT_CONTENT'] = ['json']

app.conf.update(
	#setting cron schedule
	CELERYBEAT_SCHEDULE = {
		'congrats_with_birthday': {
			'task': 'bot_core.bot.detect_birthday',
			'schedule': crontab(), # every minute
		},
	}
)

CELERY_TIMEZONE = 'UTC'
# END : CELERY config
